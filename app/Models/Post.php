<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;

class Post extends Model {

	protected $fillable = ['title', 'content', 'slug', 'comment_count', 'read_more'];
 
    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function users()
    {
        return $this->belongTo('App\Models\User');
    }
 
}
