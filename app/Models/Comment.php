<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	protected $fillable = ['commenter', 'comment', 'email'];

	public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

}
