<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/* Home routes */

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::filter('auth', function(){
    if (Auth::guest()) return Redirect::guest('login');
});

/* Model Bindings */
Route::model('post','App\Models\Post');
Route::model('comment','App\Models\Comment');
Route::model('user','App\Models\User');
 
/* User routes */
Route::get('/post/{post}/show',['as' => 'post.show','uses' => 'PostController@showPost']);
Route::post('/post/{post}/comment',['as' => 'comment.new','uses' =>'CommentController@newComment']);
Route::get('/post/feed',['as' => 'post.feed','uses' => 'PostController@postJsonFeed']);

Route::get('/author/{user}',['as' => 'author','uses' => 'BlogController@postByAuthor']);

Route::post('/login', ['as' => 'login', 'uses' => 'AuthController']);

/* Admin routes */
Route::group(['prefix' => 'admin','before'=>'auth'],function(){
   
    /*get routes*/
    Route::get('dash-board', function(){

        $user = Auth::user();

        return view('dash')->with([
        	'content' => '<h1>Hi <strong>' . $user->name . '</strong>, Welcome to Your Dashboard!</h1>',
        	'title' => 'DashBoard'
        ]);
 
    });
    Route::get('/post/list',['as' => 'post.list','uses' => 'PostController@listPost']);
    Route::get('/post/new',['as' => 'post.new','uses' => 'PostController@newPost']);
    Route::get('/post/{post}/edit',['as' => 'post.edit','uses' => 'PostController@editPost']);
    Route::get('/post/{post}/delete',['as' => 'post.delete','uses' => 'PostController@deletePost']);
    Route::get('/comment/list',['as' => 'comment.list','uses' => 'CommentController@listComment']);
    Route::get('/comment/{comment}/show',['as' => 'comment.show','uses' => 'CommentController@showComment']);
    Route::get('/comment/{comment}/delete',['as' => 'comment.delete','uses' => 'CommentController@deleteComment']);
 
    /*post routes*/
    Route::post('/post/save',['as' => 'post.save','uses' => 'PostController@savePost']);
    Route::post('/post/{post}/update',['as' => 'post.update','uses' => 'PostController@updatePost']);
    Route::post('/comment/{comment}/update',['as' => 'comment.update','uses' => 'CommentController@updateComment']);
 
});

Route::controller('/','BlogController');

 
