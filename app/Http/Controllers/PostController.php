<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

use HTML;
use Input;
use Validator;
use Redirect;
use Response;
use Auth;

class PostController extends Controller {

	 /* get functions */
    public function listPost()
    {
        $user = Auth::user();
        $posts = Post::where('post_author', $user->id)->orderBy('id','desc')->paginate(10);
        $title = 'Post listings';
        return view('dash')->nest('content','posts.list',compact('posts'))->with('title', $title);
    }

    public function postJsonFeed()
    {
        $posts = Post::orderBy('id','desc')->paginate(10);
        $title = 'Post listings';
        return Response::json($posts);
    }

    public function showPost(Post $post)
    {   
        $comments = $post->comments()->where('approved', '=', 1)->get();
        $title = $post->title;
        return view('posts.single')->with(compact('post', 'comments', 'title'));
    }
 
    public function newPost()
    {
        $title = 'New Post';
        return view('dash')->nest('content', 'posts.new')->with('title', $title);
    }
 
    public function editPost(Post $post)
    {
        $title = 'Edit Post';
        return view('dash')->nest('content', 'posts.edit', compact('post'))->with('title', $title);
    }
 
    public function deletePost(Post $post)
    {
        $post->delete();
        return Redirect::route('post.list')->with('success', 'Post is deleted!');
    }
 
    /* post functions */
    public function savePost()
    {
        $user = Auth::user();
        $post = [
            'title' => Input::get('title'),
            'content' => Input::get('content'),
        ];
        $rules = [
            'title' => 'required',
            'content' => 'required',
        ];
        $valid = Validator::make($post, $rules);
        if ($valid->passes())
        {
            $post = new Post($post);
            $post->slug = str_replace( ' ', '-', strtolower($post['title']) );
            $post->post_author = $user->id;
            $post->comment_count = 0;
            $post->read_more = (strlen(strip_tags($post->content)) > 300) ? substr(strip_tags($post->content), 0, 300) : strip_tags($post->content);
            $post->content = HTML::entities($post->content);
            $post->save();
            return Redirect::to('admin/dash-board')->with('success', 'Post is saved!');
        }
        else
            return Redirect::back()->withErrors($valid)->withInput();
    }
 
    public function updatePost(Post $post)
    {
        $data = [
            'title' => Input::get('title'),
            'content' => Input::get('content'),
        ];
        $rules = [
            'title' => 'required',
            'content' => 'required',
        ];
        $valid = Validator::make($data, $rules);
        if ($valid->passes())
        {
            $post->title = $data['title'];
            $post->slug = str_replace( ' ', '-', strtolower($data['title']) );
            $post->content = $data['content'];
            $post->read_more = (strlen(strip_tags($post->content)) > 300) ? substr(strip_tags($post->content), 0, 300) : strip_tags($post->content);
            $post->content = HTML::entities($post->content);
            if(count($post->getDirty()) > 0) /* avoiding resubmission of same content */
            {
                $post->save();
                return Redirect::back()->with('success', 'Post is updated!');
            }
            else
                return Redirect::back()->with('success','Nothing to update!');
        }
        else
            return Redirect::back()->withErrors($valid)->withInput();
    }

}
