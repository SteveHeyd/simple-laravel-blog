<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Comment;

use Illuminate\Http\Request;
use Input;
use Validator;
use Redirect;
use URL;
use HTML;

class CommentController extends Controller {

	
    /* get functions */
    public function listComment()
    {
        $comments = Comment::orderBy('id','desc')->paginate(20);
        $title = 'Comment Listings';
        return view('dash')->nest('content','comments.list',compact('comments'))->with('title', $title);
    }
 
    public function newComment(Post $post)
    {
        $comment = [
            'commenter' => Input::get('commenter'),
            'email' => Input::get('email'),
            'comment' => HTML::entities( Input::get('comment') ),
        ];
        $rules = [
            'commenter' => 'required',
            'email' => 'required | email',
            'comment' => 'required',
        ];
        $valid = Validator::make($comment, $rules);
        if($valid->passes())
        {
            $comment = new Comment($comment);
            $comment->approved = 'no';
            $post->comments()->save($comment);
            /* redirect back to the form portion of the page */
            return Redirect::to(URL::previous().'#reply')
                    ->with('success','Comment has been submitted and waiting for approval!');
        }
        else
        {
            return Redirect::to(URL::previous().'#reply')->withErrors($valid)->withInput();
        }
    }
 
    public function showComment(Comment $comment)
    { 
            return view('comments.show',compact('comment'));
    }
 
    public function deleteComment(Comment $comment)
    {
        $post = $comment->post;
        $status = $comment->approved;
        $comment->delete();
        ($status === 'yes') ? $post->decrement('comment_count') : '';
        return Redirect::back()->with('success','Comment deleted!');
    }
 
    /* post functions */
 
    public function updateComment(Comment $comment)
    {
        $comment->approved = Input::get('status');
        $comment->save();
        $comment->post->comment_count = Comment::where('post_id','=',$comment->post->id)
            ->where('approved','=',1)->count();
        $comment->post->save();
        return Redirect::back()->with('success','Comment '. (($comment->approved === '1') ? 'Approved' : 'Disapproved'));
    }

}
