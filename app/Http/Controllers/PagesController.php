<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	public function index(){

		return view('pages.index')->with([
			'title' => 'A Simple Blog',
		]);
		
	}

	public function about(){
		$name = 'Steve Heydweiller';

		return view('pages.about')->with([
			'name' => $name,
			'title' => 'About Us',
		]);
	}

}
