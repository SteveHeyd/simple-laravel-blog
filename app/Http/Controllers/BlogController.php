<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use App\Models\Comment;
use Input;
use Validator;
use Auth;
use View;
use Redirect;

use Illuminate\Http\Request;

class BlogController extends Controller {

	public function __construct()
    {

        //updated: prevents re-login.
        $this->beforeFilter('guest',['only' => ['getLogin']]);
        $this->beforeFilter('auth',['only' => ['getLogout']]);
    }
    public function getIndex() 
    {
        $posts = Post::orderBy('id','desc')->paginate(10);
        // For Laravel 4.2 use getFactory() instead of environment() method.
        $title = 'Home Page | Laravel Blog';
        $pageHeadline = 'No Posts Where Found';
        if( count($posts) ) $pageHeadline = 'Welcome to this Blog of Nothing!';
        return view('index')->with(compact('posts', 'title', 'pageHeadline'));

    }
    public function postByAuthor(User $user) 
    {
        $posts = Post::where('post_author', $user->id)->orderBy('id','desc')->paginate(10);
        // For Laravel 4.2 use getFactory() instead of environment() method.
        $title = 'Posts by ' . $user->name;
        $pageHeadline = 'No Posts Where Found for ';
        if( count($posts) > 0 ) $pageHeadline = 'Showing Posts Belonging to ';
        $pageHeadline .= $user->name;
        return view('index')->with(compact('posts', 'title','pageHeadline'));

    }
    public function getSearch()
    {
        $searchTerm = Input::get('s');
        $posts = Post::whereRaw('match(title,content) against(? in boolean mode)',[$searchTerm])
                     ->paginate(10);
        // For Laravel 4.2 use getFactory() instead of environment() method.
        $posts->setPageName('pagination::slider');
        $posts->appends(['s'=>$searchTerm]);
        $this->layout->with('title','Search: '.$searchTerm);
        $this->layout->main = View::make('home')
                     ->nest('content','index',($posts->isEmpty()) ? ['notFound' => true ] : compact('posts'));
    }
 
    public function getLogin() 
    {
        $title='Login';
        return view('auth.login')->with('title', $title);
    }
 
    public function postLogin()
    {
        $credentials = [
            'name' => Input::get('username'),
            'password' => Input::get('password')
        ];
        $rules = [
            'name' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($credentials,$rules);
        if($validator->passes())
        {
            if(Auth::attempt($credentials))
                return Redirect::to('admin/dash-board');
            return Redirect::back()->withInput()->with('failure','username or password is invalid!');
        }
        else
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }
 
    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }
    
}
