@extends('master')

@section('content')

	<div class="center error clearfix">

		<span class="warning"><i class="fi-alert"></i></span>

		<h1>The page your a looking does not exist.</h1>

		<p>Look at you, trying to see something that does not exist.</p>

	</div>

@stop