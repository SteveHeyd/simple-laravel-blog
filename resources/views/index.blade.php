@extends('master')

@section('content')

<h1 class="page-headline">{{ $pageHeadline }}</h1>

@if( count($posts) )

    @foreach($posts as $post)

        <article class="post list">

            <div class="center">
        
                <header class="post-header">
            
                    <h1 class="post-title">
            
                        {!! link_to_route('post.show',$post->title,$post->id) !!}

                        <small class="right date">{{ date('F j, Y', strtotime($post->created_at)) }} |  

                        {{-- Comment or Comments --}}
                        @if( $post->comment_count === 1 ) 

                            <i class="fi-comment"></i>
            
                        @elseif( $post->comment_count === 0 ) 

                            <i class="fi-comment-minus"></i>

                        @else 

                            <i class="fi-comments"></i>
            
                        @endif {{ $post->comment_count }}</small>
            
                    </h1>
                
                </header>
                
                <div class="post-content">
                
                    <p>{{ $post->read_more.' ...' }}</p>
                
                
                </div>
                
                <footer class="post-footer">
                
                    {!! link_to_route('post.show','Read full article',$post->id,['class'=> 'button expand']) !!}

                </footer>

            </span>
        
        </article>

    @endforeach

    <div class="center clearfix">

        {!! $posts->render() !!}

    </div>

@endif

@stop
