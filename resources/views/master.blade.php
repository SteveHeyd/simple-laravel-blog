<!DOCTYPE html>

<html class="no-js" lang="en">

<head>
    
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width">
    
    @section('title')
        <title>{{ $title }}</title>
    @show
    
    {!! HTML::style('css/foundation.css') !!}
    
    {!! HTML::style('css/foundation-icons.css') !!}
    
    {!! HTML::style('css/app.css') !!}
    
    {!! HTML::script('js/vendor/custom.modernizr.js') !!}
    
    {!! HTML::script('js/vendor/jquery.js') !!}

    @yield('head')

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-50516223-2', 'auto');
      ga('send', 'pageview');

    </script>
    
</head>

    <body>

        <header id="masthead">
        
            <nav class="top-bar">
        
                <ul class="title-area">
        
                    <!-- Title Area -->
                    <li class="name"></li>
        
                    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        
                </ul>
        
                <section class="top-bar-section">
        
                    <ul class="left">
        
                        <li class="{!! (strcmp(URL::full(), URL::to('/')) == 0) ? 'active' : ''!!}"><a href="{!! URL::to('/')!!}"><i class="fi-home"></i> The Blog of a Web Ninja</a></li>
        
                        @if( isset($users) && count($users) )

                            <li class="has-dropdown">

                                <a><i class="fi-torsos-male-female"></i> Authors</a>

                                <ul class="dropdown">

                                    @foreach( $users as $user )

                                        <li>

                                            <a href="{{ route('author', array($user->id)) }}">{{ $user->name }}</a>

                                        </li>

                                    @endforeach

                                </ul>

                            </li>

                        @endif

                    </ul>
                     
                    <ul class="right">

                    @if(Auth::check())

                        @if( isset($currentUser) )

                        <li>

                            <a>Welcome {{ $currentUser->name }}!</a>

                        </li>

                        @endif
                    
                        <li class="{!! (strpos(URL::current(), URL::to('admin/dash-board'))!== false) ? 'active' : '' !!}">
                        
                            {!! HTML::link('admin/dash-board','Dashboard')!!}
                        
                        </li>
                        
                        <li class="{!! (strpos(URL::current(), URL::to('logout'))!== false) ? 'active' : '' !!}" >
                        
                            {!! HTML::link('logout','Logout')!!}
                        
                        </li>
                    
                    @else
                    
                        <li class="{!! (strpos(URL::current(), URL::to('login'))!== false) ? 'active' : '' !!}">
                            
                            {!! HTML::link('login','Login')!!}
                        
                        </li>

                        <li class="{!! (strpos(URL::current(), URL::to('auth/register'))!== false) ? 'active' : '' !!}">
                            
                            {!! HTML::link('auth/register','Register')!!}

                        </li>

                    @endif

                        <li>
                    
                            <a href="{{ url('post/feed') }}"><i class="fi-rss"></i></a>
                    
                        </li>
                    
                    </ul>
                
                </section>
            
            </nav>
        
        </header>
              
        @yield('content')
        
        <footer class="site-footer">
            
            <div class="center">

                <p>&copy; {{ date('Y') }} Steven Heydweiller | That's right it's mine, do something about it.</p>

            </div>

        </footer>

        {!! HTML::script('js/foundation.min.js') !!}

        <script>

            $(document).foundation();

        </script>

    </body>

</html>