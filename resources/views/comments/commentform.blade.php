<div id="reply">
    
    <h3>Leave a Reply</h3>
    
    @if(Session::has('success'))
    
        <div data-alert class="alert-box round">
    
            {{ Session::get('success') }}
    
            <a href="#" class="close">&times;</a>
    
        </div>
    
    @endif
    
    {!! Form::open(['route'=>['comment.new',$post->id]]) !!}
        
        {!! Form::label('commenter','Name:') !!}
        {!! Form::text('commenter',Input::old('commenter')) !!}


        {!! Form::label('email','Email:') !!}
        {!! Form::text('email',Input::old('email')) !!}
    

        {!! Form::label('comment','Comment:') !!}
        {!! Form::textarea('comment',Input::old('comment'),['rows'=>5]) !!}
         
    @if($errors->has())
       
        @foreach($errors->all() as $error)
       
            <div data-alert class="alert-box warning round">
       
                {{ $error }}
       
                <a href="#" class="close">&times;</a>
       
            </div>
       
        @endforeach
    
    @endif

    {!! Form::submit('Submit',['class'=>'button expand large radius']) !!}

    {!! Form::close() !!}

</div>
