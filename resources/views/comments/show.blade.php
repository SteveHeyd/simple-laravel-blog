
<p><b>Commenter:</b> {{$comment->commenter}}</p>

<p><b>Email:</b> {{$comment->email}}</p>

<p><b>Comment:</b></p>

<p>{!! HTML::decode( $comment->comment ) !!}</p>

<a class="close-reveal-modal">&#215;</a>
