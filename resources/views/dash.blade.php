@extends('master')

@section('head')

    {!! HTML::script('js/tinymce/tinymce.min.js') !!}

    <script type="text/javascript">

    tinymce.init({
        selector: "textarea"
    });

    </script>

@stop

@section('content')

    <div class="center dash clearfix">

        <div class="small-3 large-3 column">
            
            <aside class="sidebar">
               
                <h3>Menu</h3>
               
                <ul class="side-nav">
               
                    <li>{!! HTML::link('/','Home') !!}</li>
               
                    <li class="divider"></li>
               
                    <li class="{!!  (strpos(URL::current(),route('post.new'))!== false) ? 'active' : ''  !!}">
               
                        {!! HTML::linkRoute('post.new','New Post') !!}
               
                    </li >
               
                    <li class="{!!  (strpos(URL::current(),route('post.list'))!== false) ? 'active' : ''  !!}">
               
                        {!! HTML::linkRoute('post.list','List Posts') !!}
               
                    </li>
               
                    <li class="divider"></li>
               
                    <li class="{!!  (strpos(URL::current(),route('comment.list'))!== false) ? 'active' : ''  !!}">
               
                        {!! HTML::linkRoute('comment.list','List Comments') !!}
               
                    </li>
               
                </ul>
            
            </aside>
        
        </div>
        
        <div class="small-9 large-9 column">
        
            <div class="content">
        
                @if(Session::has('success'))
        
                <div data-alert class="alert-box">
        
                    {!! Session::get('success') !!}
        
                    <a href="#" class="close">&times;</a>
        
                </div>
        
                @endif
        
                {!! $content !!}
        
            </div>
        
            <div id="comment-show" class="reveal-modal small" data-reveal>
        
                {-- quick comment --}
        
            </div>
        
        </div>

    </div>

@stop

