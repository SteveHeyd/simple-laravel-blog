@extends('master')

@section('content')

<div class="center register clearfix">

	<h1>Reset Password</h1>
		
	@if (session('status'))
		
		<div data-alert class="alert-box">
		
			{{ session('status') }}
		
		</div>
	
	@endif

	@if (count($errors) > 0)
		<div data-alert class="alert-box alert">
			
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			
			<ul>
			
				@foreach ($errors->all() as $error)
			
					<li>{{ $error }}</li>
			
				@endforeach
			
			</ul>
		
		</div>
	@endif

	<form role="form" method="POST" action="{{ url('/password/email') }}">
	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<label>E-Mail Address</label>
	
		<input type="email" class="form-control" name="email" value="{{ old('email') }}">

		<button type="submit" class="btn btn-primary">Send Password Reset Link</button>
	
	</form>
				
</div>
@endsection
