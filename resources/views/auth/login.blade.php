@extends('master')

@section('content')

<div class="center login">

	<h1>Login</h1>
	
	@if (count($errors) > 0)
		<div data-alert class="alert-box alert">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form role="form" method="POST" action="{{ url('/auth/login') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<label>Email</label>
		<input type="text" class="form-control" name="email" value="{{ old('name') }}">
	

		<label>Password</label>
		<input type="password" class="form-control" name="password">
	
		
		<label>
			<input type="checkbox" name="remember"> Remember Me
		</label>
			

		<button type="submit" class="button">Login</button>

		<a class="button secondary" href="{{ url('/password/email', ['title'=>'Reset Password Email']) }}">Forgot Your Password?</a>
	
	</form>

</div>
	
@endsection
