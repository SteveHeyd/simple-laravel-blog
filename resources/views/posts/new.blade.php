<h2 class="new-post">
    
    <i class="fi-page-add"></i> Add New Post
    
    <span class="right">{!! HTML::link('admin/dash-board','Cancel',['class' => 'button tiny radius']) !!}</span>

</h2>

<hr>

{!! Form::open(['route'=>['post.save']]) !!}

        {!! Form::label('title','Post Title:') !!}
        {!! Form::text('title',Input::old('title')) !!}

        {!! Form::label('content','Content:') !!}
        {!! Form::textarea('content',Input::old('content'),['rows'=>10]) !!}
   
@if($errors->has())

    @foreach($errors->all() as $error)
        
        <div data-alert class="alert-box warning round">
        
            {{$error!!}
        
            <a href="#" class="close">&times;</a>
        
        </div>
    
    @endforeach

@endif

{!! Form::submit('Save',['class'=>'button large expand radius']) !!}

{!! Form::close() !!}