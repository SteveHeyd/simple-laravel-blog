<h2 class="edit-post">
    
    <i class="fi-page-edit"></i> Edit Post
    
    <span class="right">{!! HTML::linkRoute('post.list','Cancel',null,['class' => 'button tiny radius']) !!}</span>

</h2>

<hr>

{!! Form::open(['route'=>['post.update',$post->id]]) !!}


    {!! Form::label('title','Post Title:') !!}
    {!! Form::text('title',Input::old('title',$post->title)) !!}


    {!! Form::label('content','Content:') !!}
    {!! Form::textarea('content',Input::old('content',$post->content),['rows'=>10]) !!}


@if($errors->has())

    @foreach($errors->all() as $error)
       
        <div data-alert class="alert-box warning round">
       
            {{$error}}
       
            <a href="#" class="close">&times;</a>
       
        </div>

    @endforeach

@endif

{!! Form::submit('Update',['class'=>'button expand large radius']) !!}

{!! Form::close() !!}
