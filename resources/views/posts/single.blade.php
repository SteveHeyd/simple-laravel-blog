@extends('master')

@section('head')

    {!! HTML::script('js/tinymce/tinymce.min.js') !!}

    <script type="text/javascript">

        tinymce.init({
            selector: "textarea"
        });

    </script>

@stop

@section('content')

    <article class="post single">

        <div class="center clearfix">
        
            <header class="post-header">
                
                <h1 class="post-title">
                 
                    {{ $post->title }}
                   
                    <span class="right">{!! HTML::link('#reply','Reply',['class'=>'button']) !!}</span>
                
                </h1>
                
                <div class="clearfix">
                 
                    <span class="left date">Posted on {{ date('F j, Y', strtotime($post->created_at)) }}</span>
                 
                    
                
                </div>
            
            </header>
            
            <div class="post-content">
            
                @foreach (explode("\n", $post->content) as $line)
             
                  <p>{!! HTML::decode( $line ) !!}</p>
             
                @endforeach
            
            </div>

        </div>

    </article>

    <section class="comments">

        <div class="center clearfix">
        
            @if( ! $comments->isEmpty())
            
                <h2><i class="fi-comments"></i> Comments on {{ $post->title }}</h2>
            
                <ul>
             
                    @foreach($comments as $comment)
             
                        <li>
             
                            <article>
             
                                <header>
             
                                    <div class="clearfix">
             
                                        <span class="right date">{{ explode(' ',$comment->created_at)[0] }}</span>
             
                                        <span class="left commenter">{!! link_to_route('post.show',$comment->commenter,$post->id) !!}</span>
             
                                    </div>
             
                                </header>
             
                                <div class="comment-content">
             
                                    <p>{!! HTML::decode( $comment->comment ) !!}</p>
             
                                </div>
             
                                <footer>
              
                                    <hr>
             
                                </footer>
               
                            </article>
             
                        </li>

                    @endforeach
            
                </ul>
           
            @else
          
                <h2><i class="fi-comment-minus"></i> No Comments on <strong>{{$post->title}}</strong></h2>
          
            @endif
            
            <!--comment form -->
            
            @include('comments.commentform')

        </div>

    </section>

@stop